
import java.util.Scanner;

import pieces.*;

public class Main {
	
	public static void main (String[] args) {
		
		try (Scanner sc = new Scanner (System.in)) {
			int dimx, dimy, nb_pieces;
			
			System.out.println("Entre la dimension x du plateau :");
			dimx = sc.nextInt();
			System.out.println("Entre la dimension y du plateau :");
			dimy = sc.nextInt();
			
			System.out.println("Entre le nombre de pièces a disposition :");
			nb_pieces = sc.nextInt();
			
			Piece[] pieces = new Piece[nb_pieces];
			
			for (int i=0; i<nb_pieces; i++) {
				System.out.println("Quelle pièce ajouter ? (0:Barre, 1:Carre, 2:T, 3:L_droite, 4:L_gauche, 5:Z_droite, 6:Z_gauche) :");
				int c = sc.nextInt();
				switch (c) {
				case 0:
					pieces[i] = new Barre ();
					break;
				case 1:
					pieces[i] = new Carre ();
					break;
				case 2:
					pieces[i] = new T ();
					break;
				case 3:
					pieces[i] = new L_droite ();
					break;
				case 4:
					pieces[i] = new L_gauche ();
					break;
				case 5:
					pieces[i] = new Z_droite ();
					break;
				case 6:
					pieces[i] = new Z_gauche ();
					break;
				default:
					i--;
					continue;
				}
			}
			
			Plateau plateau = new Plateau (dimx, dimy, pieces);
			
			plateau.solve();
			
			System.out.println("Solution :");
			System.out.println(plateau);
		}
	}
	
}
