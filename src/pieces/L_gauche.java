package pieces;

public class L_gauche extends Piece {
	
	public L_gauche () {
		super();
		defCase(0, 0, 0);
		defCase(1, 0, 1);
		defCase(2, 0, 2);
		defCase(3, -1, 2);
	}
	
	public L_gauche  (int x1, int y1, int x2, int y2, int x3, int y3) {
		super(x1, y1, x2, y2, x3, y3);
	}
	
	public int nb_rotations_possibles () {
		return 4;
	};
	
	public L_gauche rotate() {
		int[][] rotation = getRotation();
		L_gauche res = new L_gauche (rotation[1][0], rotation[1][1], rotation[2][0], rotation[2][1], rotation[3][0], rotation[3][1]);
		return res;
	}

}
