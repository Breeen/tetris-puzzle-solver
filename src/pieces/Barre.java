package pieces;

public class Barre extends Piece {
	
	public Barre () {
		super();
		defCase(0, 0, 0);
		defCase(1, 1, 0);
		defCase(2, 2, 0);
		defCase(3, 3, 0);
	}
	
	public Barre  (int x1, int y1, int x2, int y2, int x3, int y3) {
		super(x1, y1, x2, y2, x3, y3);
	}
	
	public int nb_rotations_possibles () {
		return 2;
	}
	
	public Barre rotate() {
		int[][] rotation = getRotation();
		Barre res = new Barre (rotation[1][0], rotation[1][1], rotation[2][0], rotation[2][1], rotation[3][0], rotation[3][1]);
		return res;
	}
	
}
