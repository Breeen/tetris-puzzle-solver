package pieces;

public class Z_droite extends Piece {
	
	public Z_droite () {
		super();
		defCase(0, 0, 0);
		defCase(1, -1, 0);
		defCase(2, -1, 1);
		defCase(3, -2, 1);
	}
	
	public Z_droite  (int x1, int y1, int x2, int y2, int x3, int y3) {
		super(x1, y1, x2, y2, x3, y3);
	}
	
	public int nb_rotations_possibles () {
		return 2;
	}
	
	public Z_droite rotate() {
		int[][] rotation = getRotation();
		Z_droite res = new Z_droite (rotation[1][0], rotation[1][1], rotation[2][0], rotation[2][1], rotation[3][0], rotation[3][1]);
		return res;
	}

}