package pieces;

public abstract class Piece {
	
	protected int[][] cases;
	
	public Piece () {
		cases = new int[4][2];
	}
	
	public Piece (int x1, int y1, int x2, int y2, int x3, int y3) {
		cases = new int[4][2];
		defCase(0, 0, 0);
		defCase(1, x1, y1);
		defCase(2, x2, y2);
		defCase(3, x3, y3);
	}
	
	// Permet de définir les coordonnées relatives des cases
	protected void defCase (int i, int x, int y) {
		assert (i > 0 && i < 4);
		cases[i] = new int[] {x,y};
	}
	
	public int[] getCase (int i) {
		assert (i > 0 && i < 4);
		return cases[i];
	}
	
	public int nb_rotations_possibles () {
		return 0;
	};
	
	// Rotation avec matrice de rotation à -90°
	// Return (indice) -> (x, y)
	protected int[][] getRotation() {
		int x, y;
		int[][] res = new int[4][2];
		res[0] = new int[] {0,0};
		for (int i=1; i<4; i++) {
			x = cases[i][0];
			y = cases[i][1];
			
			int new_x = 1 * y;
			int new_y = -1 * x;
			
			res[i][0] = new_x;
			res[i][1] = new_y;
		}
		return res;
	}
	
	public abstract Piece rotate();
	
	public void copie(Piece p) {
		cases = new int[4][2];
		defCase(0, 0, 0);
		defCase(1, p.cases[1][0], p.cases[1][1]);
		defCase(2, p.cases[2][0], p.cases[2][1]);
		defCase(3, p.cases[3][0], p.cases[3][1]);
	}
	
}
