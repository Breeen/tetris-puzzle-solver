package pieces;

public class T extends Piece {
	
	public T () {
		super();
		defCase(0, 0, 0);
		defCase(1, 1, 0);
		defCase(2, 2, 0);
		defCase(3, 1, 1);
	}
	
	public T  (int x1, int y1, int x2, int y2, int x3, int y3) {
		super(x1, y1, x2, y2, x3, y3);
	}
	
	public int nb_rotations_possibles () {
		return 4;
	};
	
	public T rotate() {
		int[][] rotation = getRotation();
		T res = new T (rotation[1][0], rotation[1][1], rotation[2][0], rotation[2][1], rotation[3][0], rotation[3][1]);
		return res;
	}

}