
import java.util.Arrays;
import java.util.ArrayList;

import pieces.*;

public class Plateau {
	
	public static int NB_TRIES = 100000;
	
	private int dimx, dimy;
	private int[][] plateau;
	private Piece[] pieces;
	
	/*
	 *  0-----> (x)
	 *  |
	 *  |
	 *  V
	 * (y)
	*/
	public Plateau (int _dimx, int _dimy, Piece[] _pieces) {
		dimx = _dimx;
		dimy = _dimy;
		plateau = new int[dimx][dimy];
		clear();
		
		pieces = _pieces;
	}
	
	// Résoud le problème tetris
	public boolean solve() {

		// On copie en ArrayList la liste des pièces
		ArrayList<Piece> piecesArrayList = new ArrayList<Piece> (Arrays.asList(pieces));
		
		return solveRecur(0, piecesArrayList);
	}
	
	// Par récurrence
	private boolean solveRecur (int id, ArrayList<Piece> piecesArrayList) {
		
		// Cas d'arrêt : si la liste est vide alors le problème est résolu
		if (piecesArrayList.size() == 0) return true;
		
		// Pour chaque pièce disponible
		for (int i=0; i<piecesArrayList.size(); i++) {
			Piece p = piecesArrayList.get(i);
			
			int nb_rot = p.nb_rotations_possibles();
			
			for (int rot=0; rot<nb_rot; rot++) {
				// Tentative de placement
				int[] res = try_piece(id, p, rot);
			
				// Si réussi
				if (res[0] == 1) {
					// Copie de la liste pour éviter les problèmes
					@SuppressWarnings("unchecked")
					ArrayList<Piece> piecesArrayList2 = (ArrayList<Piece>) piecesArrayList.clone();
					// On enlève l'objet courant pour l'appel récurcif
					piecesArrayList2.remove(i);
					
					int x = res[1];
					int y = res[2];
					int rotation = res[3];
					
					// On écrit la pièce sur la plateau
					writePiece(id, x, y, p, rotation);
					
					// Si les appels récurcifs réussissent alors on renvoie vrai
					if (solveRecur(id+1, piecesArrayList2)) {
						return true;
					}
					else {
						// Sinon on supprime la pièce du plateau
						removePiece(id);
					}
				}
			}
		}
		
		return false;
	}
	
	// Test sur les emplacements disponibles si la pièce peut rentrer
	// Si elle entre nulle part -> (succes = 0)
	// Return (success, x, y, rotation)
	private int[] try_piece (int i, Piece p, int rotation) {
		int[] res = new int[] {0, 0, 0, 0};
		
		// On fait la rotation
		for (int rot=0; rot<rotation; rot++) {
			p = p.rotate();
		}
		
		// Pour toutes les cases du plateau
		for (int x=0; x<dimx; x++) {
			for (int y=0; y<dimy; y++) {
				
				// Si la case n'est pas vide on la skip
				if (plateau[x][y] != -1) continue;
					
				// Si l'emplacement est libre alors on retourne
				if (checkPlace(i, x, y, p)) {
					res = new int[] {1, x, y, rotation};
					return res;
				}		
			}
		}
		
		return res;
	}
	
	// Vérifie que la case peut se placer à l'emplacement (x, y)
	// En vérifiant aussi la hashMap des emplacements bannis
	private boolean checkPlace (int index, int x, int y, Piece piece) {
		for (int i=0; i<4; i++) {
			int[] coorCase = piece.getCase(i);
			int xCase = x + coorCase[0];
			int yCase = y + coorCase[1];

			if (xCase < 0 || xCase >= dimx || yCase < 0 || yCase >= dimy) return false;
			if (plateau[xCase][yCase] != -1) return false;
		}

		return true;
	}
	
	private void writePiece (int index, int x, int y, Piece p, int rotation) {
		for (int i=0; i<rotation; i++) {
			p = p.rotate();
		}
		
		for (int i=0; i<4; i++) {
			int[] coorCase = p.getCase(i);
			int new_x = x + coorCase[0];
			int new_y = y + coorCase[1];
			plateau[new_x][new_y] = index;
		}
//		System.out.println(this);
	}
	
	private void removePiece (int index) {
		for (int x=0; x<dimx; x++) {
			for (int y=0; y<dimy; y++) {
				if (plateau[x][y] == index) plateau[x][y] = -1;
			}
		}
	}
	
	private void clear() {
		for (int x=0; x<dimx; x++) {
			for (int y=0; y<dimy; y++) {
				plateau[x][y] = -1;
			}
		}
	}
	
	@Override
	public String toString() {
		String res = "";
		for (int y=0; y<dimy; y++) {
			for (int x=0; x<dimx; x++) {
			res += plateau[x][y] + " ";
			}
			res += "\n";
		}
		return res;
	}
	
}
